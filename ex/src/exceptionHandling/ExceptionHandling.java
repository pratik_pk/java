/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionHandling;

/**
 *
 * @author Pratik
 */
public class ExceptionHandling {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int a = 35;
        try{
            int b = a/0;
            System.out.println(b);
        }
        catch(Exception e){
            System.out.println(e);
            
        }
        System.err.println("Program terminates here.");
    }
    
}
