/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

/*
E D C B A
E D C B A
E D C B A
E D C B A
E D C B A
 */
public class Pattern_09 {
    public static void main(String[] args) {
        for(int row=1; row<=5; row++) {
            for(char ch='E'; ch>='A'; ch--) {
                System.out.print(ch+" ");
            }
            System.out.println();
        }
    }
}
