/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;
/*

        *
      * * 
    * * *
  * * * *
* * * * *

 */
public class Pattern_24 {
    public static void main(String[] args) {
        int z = 1;
        System.out.println("Enter a digit from 1 to 20");
        Scanner sc = new Scanner(System.in);
        int digit = sc.nextInt();
        for(int row=1; row<=digit; row++) {
            for(int col=digit-1; col>=row; col--) {
                System.out.print("  ");
                
            }
            for(int k=1; k<=z; k++) {
                System.out.print("* ");
            }
            z++;
            System.out.println();
        }
    }
}
