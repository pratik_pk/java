/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;
/*

*
**
***
****
*****

 */
public class Pattern_10 {
    public static void main(String[] args) {
        System.out.println("Enter a digit from 2 to 20");
        Scanner sc = new Scanner(System.in);
        int digit = sc.nextInt();
        for(int row=0; row<digit; row++) {
            for(int col=row; col>=0; col--) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
