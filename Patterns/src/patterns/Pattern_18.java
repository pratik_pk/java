/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

/*

A A A A A
B B B B
C C C
D D
E

 */
public class Pattern_18 {
    public static void main(String[] args) {
        for(int row=0; row<5; row++) {
            for(int col=5; col>row; col--) {
                System.out.print((char)(row+65)+" ");
            }
            System.out.println();
        }
    }
}
