/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

/*
E E E E E
D D D D D
C C C C C
B B B B B
A A A A A
 */
public class Pattern_08 {
    public static void main(String[] args) {
        for(char ch='E'; ch>='A'; ch--) {
            for(char c='E'; c>='A'; c--) {
                System.out.print(ch+" ");
            }
            System.out.println();
        }
    }
}
