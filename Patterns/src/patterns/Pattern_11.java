/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;

/*

1
2 2
3 3 3
4 4 4 4
5 5 5 5 5

 */
public class Pattern_11 {
    public static void main(String[] args) {
        System.out.println("Enter a digit from 2 to 20");
        Scanner sc = new Scanner(System.in);
        int digit = sc.nextInt();
        for(int row=1; row<=digit; row++) {
            for(int col=row; col>=1; col--) {
                System.out.print(row+" ");
            }
            System.out.println();
        }
    }
}
