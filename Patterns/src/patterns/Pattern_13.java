/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
/*

A
B B
C C C
D D D D
E E E E E

 */
public class Pattern_13 {
    public static void main(String[] args) {
        for(char ch='A'; ch<='E'; ch++) {
            for(char c='A'; c<=ch; c++) {
                System.out.print(ch+" ");
            }
            System.out.println();
        }
    }
}
