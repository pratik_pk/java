/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

import java.util.Scanner;

/*
5 4 3 2 1
5 4 3 2 1
5 4 3 2 1
5 4 3 2 1
5 4 3 2 1
 */
public class Pattern_07 {
    public static void main(String[] args) {    
        // TODO code application logic here     
        System.out.println("Enter a choice");   
        Scanner sc = new Scanner(System.in);    
        int choice = sc.nextInt();              
        for(int row=0; row<choice; row++) {
            for(int col=choice; col>0; col--) {
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
}
