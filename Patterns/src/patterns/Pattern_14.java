/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
/*

A
A B
A B C 
A B C D
A B C D E

 */
public class Pattern_14 {
    public static void main(String[] args) {
        for(char ch='A'; ch<='E'; ch++) {
            for(char c='A'; c<=ch; c++) {
                System.out.print(c+" ");
            }
            System.out.println();
        }
    }
}
