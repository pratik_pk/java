/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

/**
A B C D E
A B C D E
A B C D E
A B C D E 
A B C D E
*/
public class Pattern_05 {
    public static void main(String[] args) {    
                    
        for(char row='A'; row<='E'; row++) {
            for(char col='A'; col<='E'; col++) {
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
}
