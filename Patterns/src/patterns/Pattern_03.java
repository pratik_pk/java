/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

import java.util.Scanner;

/*
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
1 2 3 4 5
*/
public class Pattern_03 {
    public static void main(String[] args) {    
        // TODO code application logic here     
        System.out.println("Enter a choice");   
        Scanner sc = new Scanner(System.in);    
        int choice = sc.nextInt();              
        for(int row=1; row<=choice; row++) {
            for(int col=1; col<=choice; col++) {
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
}
