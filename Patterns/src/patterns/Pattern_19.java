/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;
/*

A B C D E
A B C D
A B C 
A B 
A

 */
public class Pattern_19 {
    public static void main(String[] args) {
        System.out.println("Enter a digit from 1 to 26");
        Scanner sc = new Scanner(System.in);
        int digit = sc.nextInt();
        for(int row=digit; row>0; row--) {
            for(int col=0; col<row; col++) {
                System.out.print((char)(col+65)+" ");
            }
            System.out.println();
        }
    }
}
