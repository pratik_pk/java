/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;
/* Pattern
1 1 1 1 1
2 2 2 2 2
3 3 3 3 3
4 4 4 4 4
5 5 5 5 5
*/
public class Pattern_02 {
    public static void main(String[] args) {        
        System.out.println("Enter a choice");       
        Scanner sc = new Scanner(System.in);        
        int choice = sc.nextInt();                  
        for(int row=1; row<=choice; row++) {        
            for(int col=1; col<=choice; col++) {
                System.out.print(row+" ");
            }
            System.out.println();
        }
    }
}
