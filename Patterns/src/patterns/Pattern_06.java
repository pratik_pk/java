/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

import java.util.Scanner;

/*
5 5 5 5 5 
4 4 4 4 4
3 3 3 3 3
2 2 2 2 2
1 1 1 1 1
 */
public class Pattern_06 {
    public static void main(String[] args) {    
        // TODO code application logic here     
        System.out.println("Enter a choice");   
        Scanner sc = new Scanner(System.in);    
        int choice = sc.nextInt();              
        for(int row=choice; row>0; row--) {
            for(int col=0; col<choice; col++) {
                System.out.print(row+" ");
            }
            System.out.println();
        }
    }
}
