/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;

import java.util.Scanner;
/* Pattern
* * * * *
* * * * *
* * * * *
* * * * *
* * * * *
*/
public class Pattern_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {    
        // TODO code application logic here     
        System.out.println("Enter a choice");   
        Scanner sc = new Scanner(System.in);    
        int choice = sc.nextInt();              
        for(int row=0; row<choice; row++) {
            for(int col=0; col<choice; col++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    
}
