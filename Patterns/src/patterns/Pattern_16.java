/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patterns;
import java.util.Scanner;
/*

1 1 1 1 1
2 2 2 2
3 3 3 
4 4 
5

 */
public class Pattern_16 {
    public static void main(String[] args) {
        System.out.println("Enter a digit from 2 to 20");
        Scanner sc = new Scanner(System.in);
        int digit = sc.nextInt();
        for(int row=1; row<=digit; row++) {
            for(int col=row; col<=digit; col++) {
                System.out.print(row+" ");
            }
            System.out.println();
        }
    }
}
