
package pkg30daysofcode;
import java.util.*;
import java.math.*;
/**
 *
 * @author Pratik
 */
public class Day2_Operators {
    Scanner scan = new Scanner(System.in);
        double mealCost = scan.nextDouble(); // original meal price
        int tipPercent = scan.nextInt(); // tip percentage
        int taxPercent = scan.nextInt(); // tax percentage
        double total,tip,tax;
        //scan.close();
      
        // Write your calculation code here.
        tip = mealCost*(tipPercent/100.00);
        tax = mealCost*(taxPercent/100.00);
        total = mealCost + tip + tax;
        
        // cast the result of the rounding operation to an int and save it as totalCost 
        int totalCost = (int) Math.round(total);
      
        // Print your result
        System.out.println("The total meal cost is" +totalCost+"dollars.");
    
}
