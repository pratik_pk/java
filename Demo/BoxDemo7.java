class Box{
	double width;
	double height;
	double length;

	// This is the constructor for Box.
	Box(double w, double h, double l){
		width = w;
		height = h;
		length = l;
	}

	// Compute and return volume
	double volume(){
		return width*height*length;
	}
}

class BoxDemo7{
	public static void main(String[] args) {
		
	
		// Declare, allocate and initialize Box objects
		Box myBox1 = new Box(10,5,15);
		Box myBox2 = new Box(5,10,15);

		double vol;

		// Get volume for first box
		vol = myBox1.volume();
		System.out.println("Volume is "+vol);

		// Get volume for second box
		vol = myBox2.volume();
		System.out.println("Volume is "+vol);
	}
}