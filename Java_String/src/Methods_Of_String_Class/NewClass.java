/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods_Of_String_Class;
//import java.lang.String;
/**
 *
 * @author prati
 */
public class NewClass {
    public static void main(String[] args) {
        int a = 10;
        String s3 = String.valueOf(a);
        String s4 = "Java is Programming Language. Java is plateform. Java is an Island.";
        String replaceString = s4.replace("Java","Kava");
        String s = "Sachin";
        String s2 = s.intern();
        String str = " Tendulkar ";
        System.out.println(s.toUpperCase());
        System.out.println(s.toLowerCase());
        System.out.println(s);
        System.out.println(str);
        System.out.println(str.trim());
        System.out.println(s.startsWith("Sa"));
        System.out.println(s.startsWith("Te"));
        System.out.println(s.endsWith("n"));
        System.out.println(s.charAt(0));
        System.out.println(s.charAt(5));
        System.out.println(s.length());
        System.out.println(str.length());
        System.out.println(s2);
        System.out.println(s3+a);
        System.out.println(replaceString);
    }
}
