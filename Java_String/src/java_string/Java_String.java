/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java_string;

/**
 *
 * @author prati
 */
public class Java_String {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        char[] ch = {'H','E','L','L','O'};
        String s1 = new String(ch);
        String s2 = "WORLD";
        String s3 = "WORLD";    // VALID
        String s4 = new String("HELLO");
        System.out.print(s1+" ");
        System.out.println(s2);
        System.out.print(s4+" ");
        System.out.println(s3);
        System.out.println(s1.length());
        System.out.println(s1.substring(2));    // String substring(int beginIndex)
    }
}
    
