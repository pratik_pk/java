/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test_String_Comparison;

/**
 *
 * @author prati
 */
public class Test_String_Comparison2 {
    public static void main(String[] args) {
        String s1 = "Sachin";
        String s2 = "SACHIN";
        String s3 = "Sachin";
        String s4 = "Tendulkar";
        System.out.println(s1.equals(s3));
        System.out.println(s1.equals(s4));
        System.out.println(s1.equalsIgnoreCase(s2));
    }
}
