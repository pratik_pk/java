/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package String_Concatenation;

/**
 *
 * @author prati
 */
public class Test_String_Concatenation1 {
    public static void main(String[] args) {
        String s = "Sachin" + " Tendulkar";
        System.out.println(s);
    }
}
//The Java compiler transforms above code to this:
//String s=(new StringBuilder()).append("Sachin").append(" Tendulkar).toString();