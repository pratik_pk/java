/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package String_Concatenation;

/**
 *
 * @author prati
 */
public class Test_String_Concatenation2 {
    public static void main(String[] args) {
        String s = 50+30+"Sachin"+40+40;
        System.out.println(s);
    }
}
//Note: After a string literal, all the "+" will be treated as string concatenation operator.