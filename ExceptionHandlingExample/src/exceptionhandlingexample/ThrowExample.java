/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionhandlingexample;

import java.io.IOException;

/**
 *
 * @author Pratik
 */
class Test {
    void m1() throws Exception{
        throw new IOException();    //checked
    }
}

public class ThrowExample{
    public static void main(String[] args){
        Test te = new Test();
        try{
            te.m1();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}